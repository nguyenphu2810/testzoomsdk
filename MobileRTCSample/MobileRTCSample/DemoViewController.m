//
//  DemoViewController.m
//  MobileRTCSample
//
//  Created by Phu on 9/14/20.
//  Copyright © 2020 Zoom Video Communications, Inc. All rights reserved.
//

#import "DemoViewController.h"
#import "SDKAuthPresenter.h"
#import "SDKInitPresenter.h"
#import "JoinMeetingUtils.h"

@interface DemoViewController ()

@property (nonatomic, retain) UIView *meetingView;

@end

@implementation DemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
            
    //
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Leave" style:UIBarButtonItemStylePlain target:self action:@selector(handleLeftBarBtn)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Join" style:UIBarButtonItemStylePlain target:self action:@selector(handleRightBarBtn)];
}
- (void) handleRightBarBtn {
    if (!self.meetingView) {
        // Join meeting
        UIView *meetingView = [[JoinMeetingUtils shared] joinMeeting:@"79802136376" withPassword:@"78KMkJ"];
        [self.view addSubview:meetingView];
        self.meetingView = meetingView;
        meetingView.frame = CGRectMake(0, 0, 300, 300);
    }
}

- (void) handleLeftBarBtn {
    if (self.meetingView) {
        [self.meetingView removeFromSuperview];
        self.meetingView = nil;
        [[JoinMeetingUtils shared] leaveCurrentMeeting];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
