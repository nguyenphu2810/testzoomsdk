//
//  SDKStartJoinMeetingPresenter+AudioServiceDelegate.m
//  MobileRTCSample
//
//  Created by Zoom Video Communications on 2018/12/5.
//  Copyright © 2018 Zoom Video Communications, Inc. All rights reserved.
//

#import "SDKStartJoinMeetingPresenter+AudioServiceDelegate.h"
#import "CustomMeetingViewController+MeetingDelegate.h"
#import "RNMeetingView+MeetingDelegate.h"

@implementation SDKStartJoinMeetingPresenter (AudioServiceDelegate)

#pragma mark - Audio Service Delegate

- (void)onSinkMeetingAudioStatusChange:(NSUInteger)userID
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onSinkMeetingAudioStatusChange:userID];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onSinkMeetingAudioStatusChange:userID];
    }
}

- (void)onSinkMeetingMyAudioTypeChange
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onSinkMeetingMyAudioTypeChange];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onSinkMeetingMyAudioTypeChange];
    }
}

- (void)onMyAudioStateChange
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onSinkMeetingAudioStatusChange:0];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onSinkMeetingAudioStatusChange:0];
    }
}

#if 0
- (void)onSinkMeetingAudioRequestUnmuteByHost
{
    NSLog(@"the host require meeting attendants to enable microphone");
}
#endif

@end
