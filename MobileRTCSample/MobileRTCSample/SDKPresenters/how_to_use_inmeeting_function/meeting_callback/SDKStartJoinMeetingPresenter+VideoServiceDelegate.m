//
//  SDKStartJoinMeetingPresenter+VideoServiceDelegate.m
//  MobileRTCSample
//
//  Created by Zoom Video Communications on 2018/12/5.
//  Copyright © 2018 Zoom Video Communications, Inc. All rights reserved.
//

#import "SDKStartJoinMeetingPresenter+VideoServiceDelegate.h"
#import "CustomMeetingViewController+MeetingDelegate.h"
#import "RNMeetingView+MeetingDelegate.h"

@implementation SDKStartJoinMeetingPresenter (VideoServiceDelegate)

#pragma mark - Video Service Delegate

- (void)onSinkMeetingActiveVideo:(NSUInteger)userID
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onSinkMeetingActiveVideo:userID];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onSinkMeetingActiveVideo:userID];
    }
}

- (void)onSinkMeetingPreviewStopped
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onSinkMeetingPreviewStopped];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onSinkMeetingPreviewStopped];
    }
}


- (void)onSinkMeetingVideoStatusChange:(NSUInteger)userID
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onSinkMeetingVideoStatusChange:userID];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onSinkMeetingVideoStatusChange:userID];
    }
}

- (void)onMyVideoStateChange
{
    if (self.customMeetingVC)
    {
        [self.customMeetingVC onMyVideoStateChange];
    }
    if (self.rnMeetingView)
    {
        [self.rnMeetingView onMyVideoStateChange];
    }
}

- (void)onSinkMeetingVideoQualityChanged:(MobileRTCNetworkQuality)qality userID:(NSUInteger)userID
{
    NSLog(@"onSinkMeetingVideoQualityChanged: %zd userID:%zd",qality,userID);
}

- (void)onSinkMeetingVideoRequestUnmuteByHost:(void (^)(BOOL Accept))completion
{
    if (completion)
    {
        completion(YES);
    }
}

@end
