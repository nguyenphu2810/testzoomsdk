//
//  JoinMeetingUtils.m
//  MobileRTCSample
//
//  Created by Phu on 9/14/20.
//  Copyright © 2020 Zoom Video Communications, Inc. All rights reserved.
//

#import "JoinMeetingUtils.h"
#import <MobileRTC/MobileRTC.h>
#import "SDKStartJoinMeetingPresenter.h"
#import "WrapperView.h"
#import "SDKAuthPresenter.h"
#import "SDKInitPresenter.h"

@interface JoinMeetingUtils()

@property (retain, nonatomic) SDKStartJoinMeetingPresenter *presenter;
@property (retain, nonatomic) NSString *pendingJoinMeetingNumber;
@property (retain, nonatomic) NSString *pendingJoinMeetingPassword;

@end

@implementation JoinMeetingUtils

+ (instancetype)shared
{
    static JoinMeetingUtils *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[JoinMeetingUtils alloc] init];
        // Do any other initialisation stuff here
        
        // SDK init
        [[[SDKInitPresenter alloc] init] SDKInit:nil];
        
        //4. MobileRTC Authorize
        [[[SDKAuthPresenter alloc] init] SDKAuth];
    });
    return sharedInstance;
}

- (SDKStartJoinMeetingPresenter *)presenter
{
    if (!_presenter)
    {
        _presenter = [[SDKStartJoinMeetingPresenter alloc] init];
    }
    
    return _presenter;
}
- (UIView *)joinMeeting:(NSString*)meetingNo withPassword:(NSString*)pwd
{
    WrapperView *rootView = [WrapperView new];
    MobileRTCMeetingService *ms = [[MobileRTC sharedRTC] getMeetingService];
    if (ms) {
        self.pendingJoinMeetingNumber = nil;
        self.pendingJoinMeetingPassword = nil;
        [self.presenter joinMeeting:meetingNo withPassword:pwd rootView:rootView];
    }
    else {
        self.pendingJoinMeetingNumber = meetingNo;
        self.pendingJoinMeetingPassword = pwd;
        self.presenter.rootView = rootView;
    }
    [rootView release];
    return self.presenter.rootView;
}
- (void) checkPendingJoinMeetingAfterAuth {
    if (self.pendingJoinMeetingNumber) {
        MobileRTCMeetingService *ms = [[MobileRTC sharedRTC] getMeetingService];
        if (ms) {
            [self.presenter joinMeeting:self.pendingJoinMeetingNumber withPassword:self.pendingJoinMeetingPassword rootView:self.presenter.rootView];
            self.pendingJoinMeetingNumber = nil;
            self.pendingJoinMeetingPassword = nil;
        }
    }
}
- (void) leaveCurrentMeeting {
    MobileRTCMeetingService *ms = [[MobileRTC sharedRTC] getMeetingService];
    if (!ms) return;
    [ms leaveMeetingWithCmd:LeaveMeetingCmd_Leave];
}
@end


