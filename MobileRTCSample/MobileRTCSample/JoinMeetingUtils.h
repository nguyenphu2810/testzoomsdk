//
//  JoinMeetingUtils.h
//  MobileRTCSample
//
//  Created by Phu on 9/14/20.
//  Copyright © 2020 Zoom Video Communications, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JoinMeetingUtils : NSObject

+ (instancetype)shared;
- (UIView *)joinMeeting:(NSString*)meetingNo withPassword:(NSString*)pwd;
- (void) leaveCurrentMeeting;
- (void) checkPendingJoinMeetingAfterAuth;

@end

NS_ASSUME_NONNULL_END
