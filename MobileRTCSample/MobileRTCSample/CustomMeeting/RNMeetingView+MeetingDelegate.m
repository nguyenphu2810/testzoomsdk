//
//  RNMeetingView+MeetingDelegate.m
//  MobileRTCSample
//
//  Created by Phu on 9/14/20.
//  Copyright © 2020 Zoom Video Communications, Inc. All rights reserved.
//

#import "RNMeetingView+MeetingDelegate.h"

@implementation RNMeetingView (MeetingDelegate)

- (void)onSinkMeetingActiveVideo:(NSUInteger)userID
{
    
}

- (void)onSinkMeetingPreviewStopped
{
}

- (void)onSinkMeetingAudioStatusChange:(NSUInteger)userID
{
    
}

- (void)onSinkMeetingMyAudioTypeChange
{
}

- (void)onSinkMeetingVideoStatusChange:(NSUInteger)userID
{
}

- (void)onMyVideoStateChange
{
}

- (void)onSinkMeetingUserJoin:(NSUInteger)userID
{
}

- (void)onSinkMeetingUserLeft:(NSUInteger)userID
{
}

- (void)onSinkMeetingActiveShare:(NSUInteger)userID
{
}

- (void)onSinkShareSizeChange:(NSUInteger)userID
{
}

- (void)onSinkMeetingShareReceiving:(NSUInteger)userID
{
}

- (void)onWaitingRoomStatusChange:(BOOL)needWaiting
{
}

- (void)onEndButtonClick:(id)sender
{
}

@end
