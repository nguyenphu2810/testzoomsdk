//
//  AppDelegate.h
//  MobileRTCSample
//
//  Created by Zoom Video Communications on 3/17/14.
//  Copyright (c) 2014 Zoom Video Communications, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
* We recommend that, you can generate jwttoken on your own server instead of hardcore in the code.
* We hardcore it here, just to run the demo.
*
* You can generate a jwttoken on the https://jwt.io/
* with this payload:
* {
*     "appKey": "string", // app key
*     "iat": long, // access token issue timestamp
*     "exp": long, // access token expire time
*     "tokenExp": long // token expire time
* }
*/
#define KjwtToken    @"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6InlXWlBlUkhNUjR5T1JfdW5VUHk4d0EiLCJleHAiOjE2MDA2NTMzNjUsImlhdCI6MTYwMDA0ODU2N30.EnV_Gy3c6nlL8BzsIOrwU-DWisx-QAOq-P9SzfEqCqw"
#define kSDKDomain   @"zoom.us"


@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate, MobileRTCPremeetingDelegate>

@property (strong, nonatomic) UIWindow *window;

- (UIViewController *)topViewController;
@end
